<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Landing Page
Route::get('/', function () {
    return view('home');
})->name('home');

// Leads (admin)
Route::get('/leads', 'LeadsController@index')->middleware('auth')->name('leads');
Route::get('/lead/{id}', 'LeadsController@single')->middleware('auth');
Route::post('/leads/new', 'LeadsController@create');

// Auth
Auth::routes();

// Thanks
Route::get('/thank-you', function () {
    return view('thanks');
})->name('thanks');
