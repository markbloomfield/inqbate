<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Mail\NewLead;
use Illuminate\Support\Facades\Mail;

class LeadsController extends Controller
{
    public function index() {
        // $leads = \App\Lead::all();
        $leads = \App\Lead::paginate(10);

        return view('leads', compact('leads'));
    }

    public function single($id) {
        $lead = \App\Lead::find($id);

        return view('lead', compact('lead'));
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:32',
            'last_name' => 'required|string|max:32',
            'street_address' => 'required',
            'province' => 'required',
            'postal_code' => 'required',
            'phone_number' => 'required',
            'email_address' => 'required|email',
            'question' => 'required',
            'terms_conditions_consent' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/')->withErrors($validator)->withInput();
        }

        $lead = new \App\Lead();
        $lead->first_name = request('first_name');
        $lead->last_name = request('last_name');
        $lead->street_address = request('street_address');
        $lead->province = request('province');
        $lead->postal_code = request('postal_code');
        $lead->phone_number = request('phone_number');
        $lead->email_address = request('email_address');
        $lead->question = request('question');
        $lead->email_opt_in = request('email_opt_in') == 1 ? true : false;
        $lead->terms_conditions_consent = request('terms_conditions_consent') == 1 ? true : false;

        $lead->save();

        Mail::to('mark@yellow-llama.com')->queue(new NewLead($lead));

        return redirect()
            ->route('thanks')
            ->with('first_name', $lead->first_name);
    }
}
