/**
 * Tooltip
 *
 * When form fields are focused, add 'active' class which will
 * show tooltip.
 *
 * Remove on blur
 */
const allFieldContainers = Array.from(document.querySelectorAll('.field'));
const allFormInputs = Array.from(
    document.querySelectorAll('input,select,textarea')
);
if (allFormInputs && allFieldContainers) {
    allFormInputs.forEach(input => {
        input.addEventListener('focus', e => {
            e.path[2].classList.add('active');
        });
        input.addEventListener('blur', e => {
            allFieldContainers.forEach(f => f.classList.remove('active'));
        });
    });
}
