@extends('layout')

@section('title', 'Home Page')

@section('location', 'Data Capture')

@section('content')

    <div id="page">

        <div id="more-details">
            <h1>We just need a few more details...</h1>

            <p>
                After submitting your information we will try to find you a
                great personalized loan offer from our lending partners.
                <br>
                This will <strong>not affect</strong> your credit score and there is no obligation.
            </p>
        </div><!-- #more-details -->

        <div id="loan-form">
            <div class="form-intro">
                <h2>Your loan details</h2>
                <p>Help us narrow down which lending partners can help you.</p>
            </div><!-- .form-intro -->

            @if ($errors->any())
                <div class="form-intro form-validation-messages">
                    <h4>This form had some errors. Please see below:</h4>

                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="post" action="/leads/new">
                {{ csrf_field() }}

                <div class="fields">
                    <div class="field">
                        <label for="first-name">First name</label>

                        <div class="input">
                            <input type="text" id="first-name" name="first_name" value="Mark" class="@error('first_name') is-invalid @enderror">
                        </div>

                        <div class="tooltip">
                            @error('first_name')
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                    </div><!-- .field -->

                    <div class="field">
                        <label for="last-name">Last name</label>

                        <div class="input">
                            <input type="text" id="last-name" name="last_name" value="Bloomfield" class="@error('last_name') is-invalid @enderror">
                        </div>

                        <div class="tooltip">
                            @error('last_name')
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                    </div><!-- .field -->

                    <div class="field">
                        <label for="street-address">Street Address</label>

                        <div class="input">
                            <textarea name="street_address" id="street-address"  class="@error('street_address') is-invalid @enderror">My address goes here</textarea>
                        </div>

                        <div class="tooltip">
                            @error('street_address')
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                    </div><!-- .field -->

                    <div class="field">
                        <label for="province">Province</label>

                        <div class="input">
                            <select name="province" id="province"  class="@error('province') is-invalid @enderror">
                                <option value="" disabled selected>Please select:</option>
                                <option value="Eastern Cape">Eastern Cape</option>
                                <option value="Free State">Free State</option>
                                <option value="Gauteng">Gauteng</option>
                                <option value="KwaZulu-Natal">KwaZulu-Natal</option>
                                <option value="Limpopo">Limpopo</option>
                                <option value="Mpumalanga">Mpumalanga</option>
                                <option value="Northern Cape">Northern Cape</option>
                                <option value="North West" selected>North West</option>
                                <option value="Western Cape">Western Cape</option>
                            </select>
                        </div>

                        <div class="tooltip">
                            @error('province')
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                    </div><!-- .field -->

                    <div class="field">
                        <label for="postal-code">Postal Code</label>

                        <div class="input">
                            <input type="number" id="postal-code" name="postal_code" value="7700" class="@error('postal_code') is-invalid @enderror">
                        </div>

                        <div class="tooltip">
                            @error('postal_code')
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                    </div><!-- .field -->

                    <div class="field">
                        <label for="phone-number">Phone Number</label>

                        <div class="input">
                            <input type="number" id="phone-number" name="phone_number" value="1234567890" class="@error('phone_number') is-invalid @enderror">
                        </div>

                        <div class="tooltip">
                            @error('phone_number')
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                    </div><!-- .field -->

                    <div class="field">
                        <label for="email">Email</label>

                        <div class="input">
                            <input type="email" id="email" name="email_address" value="mark@yellow-llama.com" class="@error('email_address') is-invalid @enderror">
                        </div>

                        <div class="tooltip">
                            @error('email_address')
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                    </div><!-- .field -->

                    <div class="field">
                        <label for="question">Question</label>

                        <div class="input">
                            <textarea name="question" id="question" class="@error('question') is-invalid @enderror">My question goes here</textarea>
                        </div>

                        <div class="tooltip">
                            @error('question')
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                    </div><!-- .field -->

                    <div class="field">
                        <label for="email-opt-in">Please send me emails</label>

                        <div class="input">
                            <input id="email-opt-in" type="checkbox" name="email_opt_in" value="1" checked >
                        </div>

                        <div class="tooltip">
                        </div>
                    </div>

                    <div class="field">
                        <label for="terms-and-conditions">I agree to the Terms & Conditions</label>

                        <div class="input">
                            <input id="terms-and-conditions" type="checkbox" name="terms_conditions_consent" value="1" checked class="@error('terms_conditions_consent') is-invalid @enderror">
                        </div>

                        <div class="tooltip">
                            @error('terms_conditions_consent')
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
                                    {{ $message }}
                                </span>
                            @enderror
                        </div>
                    </div><!-- .field -->

                </div>

                <button type="submit" class="button has-arrow has-gradient">Next <span><svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M4 .755l14.374 11.245-14.374 11.219.619.781 15.381-12-15.391-12-.609.755z"/></svg></span></button>
            </form>
        </div><!-- #loan-form -->

    </div><!-- #page -->

    <div id="terms">
        <div class="lead">
            <p>
                *APR’s on personal loan offers from the panel of lending partners on Monevo
                originated through Google or Bing advertisements range from 3.09% to 35.99%,
                with loan durations between 90 days and 144 months. Repayment examples (for
                illustrative purposes only): a $10,000 loan at 4.89% APR with a term of 3 years
                would result in 36 monthly payments of $299 (Total repayable : $10,772) and a
                $10,000 loan at 9.99% APR with a term of 5 years would result in 60 monthly
                payments of $201.81 (Total repayable : $12,108.60). Each state has specific
                rules and regulations that govern lending partners. The amount you can borrow,
                the APR, and repayment term are based on your state’s laws, the lending partner,
                and your creditworthiness.
            </p>
        </div>

        <div class="columns">
            <p>
                The operator of this website is not a lender or party to any loan or other
                transaction, does not broker loans to lending partners, and does not make
                personal loans or credit decisions. This website will submit the information
                you provide to lending partners in our database depending on the information
                provided, who will first use a soft credit check(s) to assess your eligibility
                for a personal loan. You can learn more about how our lending partners assess
                your eligibility for a loan in Section 1 of the Terms. The amount of any
                personal loan offer, if one is made, will vary depending on the lending
                partner, your creditworthiness and your state’s laws. There is no guarantee
                you will be presented with any personalized personal loan offers, or that
                upon presentation of any personalized personal loan offers you will qualify
                for the rates, fees, or terms shown on this site.
            </p>
            <p>
                This website does not constitute an offer or solicitation to borrow,
                acceptance into any particular loan program, or specific loan terms or
                conditions. Providing your information on this website does not guarantee that
                you will be approved for a personal loan.
            </p>
            <p>
                If you receive a personal loan offer from one of our lending partners it is
                imperative that you review each lender’s terms and conditions before proceeding
                with an application for a loan and please note, that at the point of
                application with the lending partner you may be subject to a full credit check.
            </p>
            <p>
                In order to help the government fight identity theft, the funding of
                terrorism and money laundering activities, lending partners may verify and
                record information that identifies you. The operator of this website is
                not an agent, representative or broker of any lender. We do not endorse or
                recommend any lending partners. We do not charge you for any service or product.
            </p>
            <p>
                The operator of this website is not a lender or party to any loan or other
                transaction, does not broker loans to lending partners, and does not make
                personal loans or credit decisions. This website will submit the information
                you provide to lending partners in our database depending on the information
                provided, who will first use a soft credit check(s) to assess your eligibility
                for a personal loan. You can learn more about how our lending partners assess
                your eligibility for a loan in Section 1 of the Terms. The amount of any
                personal loan offer, if one is made, will vary depending on the lending
                partner, your creditworthiness and your state’s laws. There is no guarantee
                you will be presented with any personalized personal loan offers, or that
                upon presentation of any personalized personal loan offers you will qualify
                for the rates, fees, or terms shown on this site.
            </p>
            <p>
                This website does not constitute an offer or solicitation to borrow,
                acceptance into any particular loan program, or specific loan terms or
                conditions. Providing your information on this website does not guarantee that
                you will be approved for a personal loan.
            </p>
            <p>
                If you receive a personal loan offer from one of our lending partners it is
                imperative that you review each lender’s terms and conditions before proceeding
                with an application for a loan and please note, that at the point of
                application with the lending partner you may be subject to a full credit check.
            </p>
            <p>
                In order to help the government fight identity theft, the funding of
                terrorism and money laundering activities, lending partners may verify and
                record information that identifies you. The operator of this website is
                not an agent, representative or broker of any lender. We do not endorse or
                recommend any lending partners. We do not charge you for any service or product.
            </p>
        </div><!-- .columns -->

        <div class="smaller">
            <p>
                Monevo Inc is licensed by the Department of Business Oversight under the
                California Financing Law, license number 60DBO-71792.
            </p>
        </div>
    </div><!-- #terms -->

@endsection
