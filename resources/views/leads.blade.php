@extends('layout')

@section('title', 'All Leads')

@section('location', 'All Leads')

@section('content')

    <div id="page">
        <h1>All Leads</h1>

        @if (count($leads) >= 1)

            <div id="leads">
                <div class="legend">
                    <div class="lead-id">ID</div>
                    <div class="full_name">Full Name</div>
                    <div class="street_address hide-on-mobile">Address</div>
                    <div class="province hide-on-mobile">Province</div>
                    <div class="postal_code hide-on-mobile">Code</div>
                    <div class="phone_number">Phone</div>
                    <div class="email_address">Email</div>
                    <div class="question hide-on-mobile">Question</div>
                    <div class="email_opt_in hide-on-mobile">Email?</div>
                    <div class="terms_conditions_consent hide-on-mobile">Terms?</div>
                </div>

                @foreach ($leads as $lead)

                    <div class="lead">
                        <div class="lead-id">{{ $lead->id }}</div>
                        <div class="full_name"><a href="/lead/{{ $lead->id }}">{{ $lead->first_name }} {{ $lead->last_name }}</a></div>
                        <div class="street_address hide-on-mobile">{{ $lead->street_address }}</div>
                        <div class="province hide-on-mobile">{{ $lead->province }}</div>
                        <div class="postal_code hide-on-mobile">{{ $lead->postal_code }}</div>
                        <div class="phone_number">{{ $lead->phone_number }}</div>
                        <div class="email_address">{{ $lead->email_address }}</div>
                        <div class="question hide-on-mobile">{{ $lead->question }}</div>
                        <div class="email_opt_in hide-on-mobile">{{ $lead->email_opt_in == 1 ? 'Yes' : 'No' }}</div>
                        <div class="terms_conditions_consent hide-on-mobile">{{ $lead->terms_conditions_consent == 1 ? 'Yes' : 'No' }}</div>
                    </div>

                @endforeach

                <div class="legend footer">
                    <div class="lead-id">ID</div>
                    <div class="full_name">Full Name</div>
                    <div class="street_address hide-on-mobile">Address</div>
                    <div class="province hide-on-mobile">Province</div>
                    <div class="postal_code hide-on-mobile">Code</div>
                    <div class="phone_number">Phone</div>
                    <div class="email_address">Email</div>
                    <div class="question hide-on-mobile">Question</div>
                    <div class="email_opt_in hide-on-mobile">Email?</div>
                    <div class="terms_conditions_consent hide-on-mobile">Terms?</div>
                </div>

            </div>

            <div id="pagination">
                {{$leads->links()}}
            </div>

        @else

            <h2>No leads found</h2>

        @endif

    </div>

@endsection
