@extends('layout')

@section('title', 'Login')

@section('content')

<div id="page">

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <form class="auth-form" method="POST" action="{{ route('password.email') }}">
        @csrf

        <div class="fields">
            <div class="field">
                <label for="email">Email Address</label>

                <div class="input">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                </div>

                <div class="tooltip">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div><!-- .field -->

            <div class="field">
                <label>&nbsp;</label>

                <div class="input">
                    <button type="submit" class="button smaller">
                        {{ __('Reset') }}
                    </button>
                </div>

                <div class="tooltip">
                </div>
            </div>
        </div><!-- #fields -->
    </form>
</div>

@endsection
