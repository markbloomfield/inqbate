@extends('layout')

@section('title', 'Login')

@section('content')

<div id="page">
    <form class="auth-form" method="POST" action="{{ route('login') }}">
        @csrf

        <div class="fields">
            <div class="field">
                <label for="email">Email Address</label>

                <div class="input">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email Address">
                </div>

                <div class="tooltip">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
            </div><!-- .field -->

            <div class="field">
                <label for="password">Password</label>

                <div class="input">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autocomplete="current-password">
                </div>

                <div class="tooltip">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div><!-- .field -->

            <div class="field">
                <label for="remember">Remember Me</label>

                <div class="input">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                </div>

                <div class="tooltip">
                </div>
            </div><!-- .field -->

            <div class="field">
                <label>&nbsp;</label>

                <div class="input">
                    <button type="submit" class="button smaller">
                        {{ __('Login') }}
                    </button>

                    @if (Route::has('password.request'))
                        <a class="forgot" href="{{ route('password.request') }}">
                            {{ __('Forgot?') }}
                        </a>
                    @endif
                </div>

                <div class="tooltip">
                </div>
            </div>
        </div><!-- #fields -->
    </form>
</div>

@endsection
