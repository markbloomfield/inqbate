@extends('layout')

@section('title', 'Thank you')

@section('location', 'Thank you')

@section('content')

    <div id="page">

        <div id="more-details" class="no-arrow">
            <h1>Thank you {{session('first_name', 'friend')}}</h1>

            <p>
                Your application will be reviewed shortly and we will be in touch.
            </p>
        </div><!-- #more-details -->

    </div>

@endsection
