@extends('layout')

@section('title', 'Lead: ' . $lead->first_name . ' ' . $lead->last_name)

@section('location', 'Single Lead')

@section('content')

    <div id="page" class="single-lead">
        <h1>{{ $lead->first_name }} {{ $lead->last_name }}</h1>

        <div class="lead">
            <div class="field street_address">
                <div class="label">Street Address</div>
                <div class="value">
                    {{ $lead->street_address }}
                </div>
            </div>
            <div class="field province">
                <div class="label">Province</div>
                <div class="value">
                    {{ $lead->province }}
                </div>
            </div>
            <div class="field postal_code">
                <div class="label">Postal Code</div>
                <div class="value">
                    {{ $lead->postal_code }}
                </div>
            </div>
            <div class="field phone_number">
                <div class="label">Phone Number</div>
                <div class="value">
                    {{ $lead->phone_number }}
                </div>
            </div>
            <div class="field email_address">
                <div class="label">Email Address</div>
                <div class="value">
                    {{ $lead->email_address }}
                </div>
            </div>
            <div class="field question">
                <div class="label">Question</div>
                <div class="value">
                    {{ $lead->question }}
                </div>
            </div>
            <div class="field email_opt_in">
                <div class="label">Email Marketing Consent</div>
                <div class="value">
                    {{ $lead->email_opt_in == 1 ? 'Yes' : 'No' }}
                </div>
            </div>
            <div class="field terms_conditions_consent">
                <div class="label">Terms Agree</div>
                <div class="value">
                    {{ $lead->terms_conditions_consent == 1 ? 'Yes' : 'No' }}
                </div>
            </div>
        </div>
    </div>


@endsection
