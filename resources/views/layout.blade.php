<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="//use.typekit.net/tmi6gke.css">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" />
</head>
<body>

    <header id="header">
        <a href="/" id="logo"><img src="{{ asset('images/logo.png') }}" alt=""></a><!-- #logo -->
    </header><!-- #header -->

    <?php if (Auth::check()) { ?>
    <div id="breadcrumb">
        <p class="links">
            You are here: <strong>@yield('location')</strong>
        </p>

        <p class="user-stuff">
            <span>Hello <?php echo Auth::user()->name; ?></span>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="{{ route('leads') }}">View Leads</a>
            &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="{{ route('logout') }}" class="logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
        </p>
    </div>
    <?php } ?>

    @yield('content')

    <footer id="footer">
        <div id="quint-group">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 65 65"><path data-name="Combined Shape" d="M32.5 65A32.509 32.509 0 0 1 19.849 2.554a32.508 32.508 0 0 1 25.3 59.892A32.3 32.3 0 0 1 32.5 65zm0-53a20.5 20.5 0 1 0 10.04 38.371l9.775 1.887-1.895-9.8A20.491 20.491 0 0 0 32.5 12z" fill="#fff" opacity=".25"/><path data-name="Fill 15" d="M32.5 42A10.258 10.258 0 0 1 22 32a10.26 10.26 0 0 1 10.5-10A10.26 10.26 0 0 1 43 32a10.258 10.258 0 0 1-10.5 10" fill="#fff" opacity=".25"/></svg>
            <span>part of</span>
            <h6>Quint Group</h6>
        </div>

        <p>
            &copy; @php echo date("Y"); @endphp Monevo. All rights reserved.
        </p>
    </footer><!-- #footer -->

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>

    <script src="{{ mix('js/app.js') }}"></script>

</body>
</html>
