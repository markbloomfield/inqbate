<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email</title>
</head>
<body>
    <p>Dear Admin</p>

    <p>You have a new lead from {{$lead->first_name}} {{$lead->last_name}} on your website.</p>

    <p>Please <a href="{{route('home')}}/lead/{{$lead->id}}">click here</a> to view it</p>

    <p>Kind regards</p>
</body>
</html>
